#include "helpabout.hpp"

HelpAbout::HelpAbout(QWidget *parent) : QDialog(parent)
{
    ui = new Ui::HelpAbout;
    ui->setupUi(this);
    setStyleSheet("QDialog {color: white; background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #aaa, stop: 1 #222);}");

    QString msg;

    msg += tr("<b>ARIM Open Source ARDOP Instant Messager Project</b><br /><br />"
              "This application is an abstract conversion "
              "of the ARIM project originally written<br />"
              "in 'C'  by Bob Cummings, NW8L<br /><br/>"
              "This software is intended to be free for use as licensed by<br />"
              "the Open Software Foundation and distribution as is.<br />"
              "It will never be sold. However, you may wish to contribute<br/>"
              "to the cost of developing this application.<br/><br/>"
              "There has been a concious attempt to leverage existing "
              "code and is working well in other applications. It is designed "
              "to be platform independent and transportable. It was "
              "completely developed in C++ using the Digia Open Source Qt libraries "
              "version 5.9.n &copy; 2017 Digia Corp.<br /><br />"
              "This ARIM Messaging software project is released under the GPL v3 or "
              "later, license.<br />"
              "&copy; Copyright 2017 Ken Standard AD5XJ ad5xj@arrl.net, <br />"
              "Bob Cummings, NW8L NW8L@arrl.net<br /><br />"
              "See the copyright notice included with the source code for"
              "more detail.<br /><br />"
              "Qt is licensed under the GNU Lesser General Public License (LGPL) "
              "version 3 and is appropriate for the development of Qt applications. "
              "This application wil comply with the terms and conditions of the "
              "GNU LGPL version 3 (or GNU GPL version 3). The License was included "
              "in the source code files of this project. If you did not receive a "
              "copy of the license, please got to http://fsf.org for a current "
              "version or write to:<br />"
              "Free Software Foundation, Inc.<br />"
              "59 Temple Place - Suite 330,<br />"
              "Boston, MA  02111-1307, USA.<br /><br />"
              "Parts of this project were derived from the Qt SDK Examples. "
              "Many thanks are due to the Digia Corp. perveyors of the Qt Framework "
              "and SDK. All applicable copyrights by Digia apply."
              );
    ui->btnOk->setStyleSheet("QPushButton {color: white; background: qlineargradient(x1: 0, y1: 0, x2: 0, y2: 1, stop: 0 #ccc, stop: 1 #555);}");
    ui->lblMsg->document()->setHtml(msg);
    ui->lblMsg->setReadOnly(true);
    setModal(true);
    setWindowFlags(Qt::FramelessWindowHint);
    setParent(0); // Create TopLevel-Widget
    setAttribute(Qt::WA_NoSystemBackground, true);
    connect(ui->btnOk,SIGNAL(clicked(bool)),this,SLOT(close()));
}

void HelpAbout::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type())
    {
    case QEvent::LanguageChange:
        ui->retranslateUi(this);
        break;
    default:
        break;
    }
}
