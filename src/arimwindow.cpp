#include <QtCore/QDebug>
#include <QSettings>
#include <QIODevice>
#include <QDataStream>
#include <QAbstractSocket>
#include <QTcpSocket>
#include <QUdpSocket>

#include "helpabout.hpp"
#include "arimwindow.hpp"


class ARIMWindow::ARIMPrivate
{
public:
    bool connected;

    quint32 fecmode;
    quint32 fecid;

    QString callsign;
    QString grid;

    HostInterface *hostintf;
};


ARIMWindow::ARIMWindow() : QMainWindow()
{
    d = new ARIMPrivate();
    ui = new Ui::ARIMWindow();
    ui->setupUi(this);

    readSettings();
    connect(ui->actionAbout_ARIM,SIGNAL(triggered(bool)),this,SLOT(slotHelpAbout()));
    connect(ui->btnSend,SIGNAL(clicked(bool)),this,SLOT(slotSendHostCMD()));

    d->hostintf = new HostInterface(this);
    connect(d->hostintf, SIGNAL(signalChgDisplay(Status)), this, SLOT(slotChgDisplay(Status)));
}

ARIMWindow::~ARIMWindow()
{
    delete d->hostintf;
    delete d;
    delete ui;
}

// ====================================================
// EVENT HANDLERS
// ====================================================


// ====================================================
// PRIVATE SLOT HANDLERS
// ====================================================
void ARIMWindow::slotSendHostCMD()
{
    QByteArray msg;

    msg.append(ui->editCMD->text().trimmed());
    qDebug() << "Formatting message..." << msg;
    d->hostintf->out(msg);
    if ( d->hostintf->hasError() )
        qDebug() << "Host Interface Error: " << d->hostintf->lasterr;
}

void ARIMWindow::slotHelpAbout()
{
    QDialog *h = new HelpAbout();
    h->show();
    h->deleteLater();
}

void ARIMWindow::slotDisplayTCPError(int socketError, QString socketErrorStr)
{
    switch (socketError)
    {
    case QAbstractSocket::RemoteHostClosedError:
        break;
    case QAbstractSocket::HostNotFoundError:
        ui->statusbar->showMessage(tr("Socket Error: The host was not found.") );
        break;
     case QAbstractSocket::ConnectionRefusedError:
        ui->statusbar->showMessage(tr("Socket Error: The connection refused by the peer.") );
        break;
     default:
        ui->statusbar->showMessage(tr("Socket Error: %1.").arg(socketErrorStr));
        break;
     }
}

void ARIMWindow::slotChgDisplay(Status status)
{
    if ( status.ControlName == "lblHost" )
    {
        QString msg;
        QString sheet;

        msg = "ARIM - Host Connection: " + status.MsgText.trimmed();
        this->setWindowTitle(msg);
        if ( status.BackColor == COLOR_LIGHTSALMON )
        {
            sheet = "QLabel { background-color: #efeeff; }";
            this->statusBar()->setStyleSheet(sheet);
        }
    }

    if ( status.ControlName == "txtTNCList" )
    { // update the TNC Output list
        QString newline = status.MsgText.trimmed();
        ui->txtTNCOut->append(newline);
    }
}


// ====================================================
// PRIVATE FRIENDLY FUNCTIONS
// ====================================================
void ARIMWindow::get_heard_list(QString *listbuf, qint32 listbufsize)
{
    Q_UNUSED(listbuf)
    Q_UNUSED(listbufsize)
    /* this function may be replaced by
     * visual objects; and methods that provide
     * data to them
     *

    qint32 i;
    qint32 len;
    qint32 cnt;

    i = 0;
    len = 0;
    cnt = 0;

    snprintf(listbuf, listbufsize, "Calls heard (%s):\n",
                last_time_heard == LT_HEARD_ELAPSED ? "ET" : "LT");
    cnt += strlen(listbuf);
    for (i = 0; i < heard_list_cnt; i++) {
        len = strlen(heard_list[i].htext);
        if ((cnt + len + 1) < listbufsize) {
            strncat(listbuf, &(heard_list[i].htext[1]), listbufsize - cnt - 1);
            cnt += len;
            strncat(listbuf, "\n", listbufsize - cnt - 1);
            ++cnt;
        } else {
            break;
        }
    }
    if ((cnt + 1) < listbufsize) {
        strncat(listbuf, "\n", listbufsize - cnt - 1);
        ++cnt;
    }
    listbuf[cnt] = '\0';
    */
}

void ARIMWindow::cmd_func(QByteArray *data)
{
    // qint32 cmdsock; // - use the socket from the settings stored locally
    Q_UNUSED(data)
    QByteArray buffer;
//    fd_set cmdreadfds, cmderrorfds;
//    qint32 rsize;

    struct addrinfo;
    struct hints;
    struct res;

    buffer.reserve(MAX_CMD_SIZE);

    //struct timeval timeout; // - use the timeout interval and method of the QTimer

//    cmdthread_queue_debug_log("Cmd thread: initializing");

    /*
     * The setup below will be replaced by the QTcpSocket objCMDSocket
     * or QUdpSocket objCMDClient setup
     *
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;  // IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM;
    getaddrinfo(g_tnc_settings[g_cur_tnc].ipaddr, g_tnc_settings[g_cur_tnc].port, &hints, &res);
    if (!res)
    {
        cmdthread_queue_debug_log("Cmd thread: failed to resolve IP address");
        g_cmdthread_stop = 1;
        pthread_exit(data);
    }
    cmdsock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (connect(cmdsock, res->ai_addr, res->ai_addrlen) == -1) {
        cmdthread_queue_debug_log("Cmd thread: failed to open TCP socket");
        g_cmdthread_stop = 1;
        pthread_exit(data);
    }
    freeaddrinfo(res);
    g_cmdthread_ready = 1;
    */
    QByteArray msg;
    msg.append("INITIALIZE\n");
    d->hostintf->out(msg);
}


void ARIMWindow::readSettings()
{
    QSettings settings("/arim.ini",QSettings::IniFormat);

    settings.beginGroup("STATION");
      d->callsign = settings.value("Callsign","unknown").toString();
      d->grid   = settings.value("GridSquare","unkn").toString();
    settings.endGroup();

    settings.beginGroup("ARDOP");
      d->fecmode  = settings.value("FECmode").toInt();
      d->fecid    = settings.value("FECid").toInt();
    settings.endGroup();
}

void ARIMWindow::saveSettings()
{
//    QDir dir; // dir.canonicalPath() + "/arim.ini"
    QSettings settings("/arim.ini",QSettings::IniFormat);

    settings.beginGroup("STATION");
      settings.setValue("Callsign",d->callsign.trimmed().toUpper());
      settings.setValue("GridSquare",d->grid.trimmed());
    settings.endGroup();

    settings.beginGroup("ARDOP");
      settings.setValue("FECmode",d->fecmode);
      settings.setValue("FECid",d->fecid);
    settings.endGroup();
    settings.sync();
}

