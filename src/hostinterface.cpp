#include <iostream>
#include <exception>
#include <math.h>
using namespace std;

#include <QDebug>
#include <QApplication>
#include <QSettings>
#include <QValidator>
#include <QRegularExpression>
#include <QRegularExpressionValidator>
#include <QNetworkSession>
#include <QNetworkConfiguration>
#include <QNetworkConfigurationManager>

#include "tcpclient.hpp"
#include "hostinterface.hpp"
#include "utils.hpp"

#define COLOR_DARKGRAY    QColor(150,150,150,255);
#define COLOR_KHAKAI      QColor(230,115,0,255);
#define COLOR_LIGHTSALMON QColor(255,179,102,255);
#define COLOR_YELLOW      QColor(255,255,0,255);

class HostInterface::HostInterfaceData
{  // local var definitions
   // use *d-ptr pointer (defined as hid) to access
public:
    bool initializing;
    bool    CONNECTED;
    bool    DATACONNECTED;
    bool    LinkedToHost;
    // Most of these are config params
    // for network or protocol
    bool    wantCWID;
    bool    CWOnOff;
    bool    DebugLog;
    bool    CommandTrace;
    bool    RadioControl;
    bool    SlowCPU;
    bool    AccumulateStats;
    bool    Use600Modes;
    bool    FSKOnly;
    bool    fastStart;
    bool    skip167;
    bool    ConsoleLogLevel;
    bool    EnablePingAck;
    bool    blnPINGrepeating;
    bool    blnFramePending;
    bool    gotGPIO;
    bool    useGPIO;

    quint32 ARQTimeout;
    quint32 bytDataToSendLen;
    quint32 hosttcpport;
    quint32 hostudpport;
    quint32 fecid;
    quint32 fecrepeats;
    quint32 squelch;
    quint32 TXLevel;
    quint32 RXLevel;
    quint32 autoRXLevel;

    qint32  LeaderLength;
    qint32  TrailerLength;
    qint32  TuningRange;
    qint32  ARQConReqRepeats;
    qint32  dttLastPINGSent;
    qint32  intPINGRepeats;
    qint32  pttGPIOPin;
    qint32  hRIGDevice;		// port for Rig Control
    qint32  RIGBAUD;          // baud rate for rig control
    qint32  hPTTDevice;       // port for PTT

    QString Callsign;
    QString GridSquare;
    QString CaptureDevice;
    QString PlaybackDevice;
//    QString RIGPORT;          // Port for rig control --- uncomment when implemented
//    QString PTTPORT;          // Port for Hardware PTT - may be same as control port.
    QString strFECMode;
    QString strInterfaceType;
    QString strLastCommandToHost;
    QString strTCPIPDataConnectionID;
    QString strTCPIPControlConnectionID;
    QString currentINData;

    QByteArray *bytDataToSend;
    QByteArray *bytLastCMD_DataSent;

    QString hosttcpaddr;
    QString callsign;
    QString gridsq;
    QString fecmode;

    QDataStream in;
    QDataStream out;

    Status  status;

    QNetworkConfiguration networkConfig;

    // local vars on the heap
    QIODevice *socketIO;


    TCPIPClient *tcpSocket; //! The TCPIP socket object retained locally
    QUdpSocket *objCMDClient; //! The UDP   socket object retained locally
//    QSerialPort *objSerial;
};

HostInterface::HostInterface(QObject *parent) : QObject(parent)
{
    hid = new HostInterfaceData;
    hid->LinkedToHost = false;

    // setup default property values //
    blnInitializing = true;
    blnTerminatingConnection = false;
    haserror = false;
    lasterr = "";
    hostip = "";
    hostname="";
    hosttcpport=0;
    hostudpport=0;
    // get current stored settings
    getSettings();
    // initialize interface with current settings
    initialize();
}


HostInterface::~HostInterface()
{
    saveSettings();

    hid->tcpSocket->deleteLater();
    QApplication::processEvents();
    delete hid;
}


// ============================================================
// PRIVATE SLOTS
// ============================================================

void HostInterface::slotReadData()
{
    hid->currentINData = hid->tcpSocket->getInData();
    hid->status.ControlName = "txtTNCOut";
    hid->status.MsgText     = hid->currentINData;
    signalChgDisplay(hid->status);
    signalDataAvailable(hid->currentINData);
    commandInterpreter();  // interpret IN data
}

// ============================================================
// FRIENDLY METHODS AND FUNCTIONS
// ============================================================
//                PUBLIC FUNCTIONS                            //
bool HostInterface::out(QByteArray msg)
{
    bool ret = SendCommandToHost(QString(msg));
    if ( lasterr == "" )
    {
        qDebug() << tr("Message sent to socket: with return")
                 << QString(msg)
                 << ret;
    }
    return ret;
}

bool HostInterface::SendCommandToHost(QString strText)
{
    qint32 len;
    QChar rtn;
    QChar lf;

    rtn = 0x13; // ascii CR char
    lf  = 0x10; // ascii LF char

    hid->bytDataToSend->clear();
    hid->bytDataToSend->append("C:");
    hid->bytDataToSend->append(strText);
    hid->bytDataToSend->append(rtn);
    len = hid->bytDataToSend->length();
    hid->bytDataToSendLen = len;
    hid->bytLastCMD_DataSent = hid->bytDataToSend;
    qDebug() << "CMD to Host is "
             << ("C:" + strText + rtn)
             << hid->bytDataToSendLen << "bytes long";


    if ( hid->strInterfaceType != "TCPIP" )
    {
        qCritical() << "TCPIP is all that is available - Invalid Interface Type " << hid->strInterfaceType;
        return false; // remove when other than TCPIP is enabled
        hid->bytDataToSend->resize(len + 1);             // resize 2 bytes larger for CRC
        GenCRC16(hid->bytDataToSend, 2, len - 3, 0xFFFF); // Generate CRC starting after "c:"
    }

    if ( hid->strInterfaceType == "TCPIP" )
    {
        qDebug() << "Interface type is TCP - no CRC required for TCPIP";
        QByteArray ba;
        ba.append(hid->bytDataToSend->data());
        qDebug() << "Writing to socket";
        hid->tcpSocket->sendCMD(QString(ba));
        if ( hid->tcpSocket->errorstr != "" )
        {
            qDebug() << "Socket error: " << hid->tcpSocket->errorstr;
            return false;
        }
        hid->strLastCommandToHost = strText.trimmed().toUpper();
        return true;
    }
    /* uncomment when this is implented
    else if ( hid->strInterfaceType == "SERIAL" )
    {
        if ( !hid->objSerial && hid->objSerial.isOpen() )
        {
            try
                objSerial.Write(bytToSend, 0, bytToSend.Length)
                Return True
            catch (ex As Exception)
            {
                Logs.Exception("[HostInterface.SendCommandToHost] Serial Port,  c:" & strText & "  Err:" & Err.Number.ToString & " " & Err.Description)
            }
        else
            Logs.Exception("[HostInterface.SendCommandToHost] Serial Port " & MCB.SerCOMPort & " not open")
        }
        return false;
    }
    else if ( hid->strInterfaceType == "BLUETOOTH" )
    {
        // This will handle BlueTooth connections ... TODO: Add BlueTooth
        return false // Temporary
    }
    */
    else
    {
//        Logs.Exception("[HostInterface.SendCommandToHost]  No TCPIP, serial,  or BlueTooth parameters")
        lasterr = "No TCPIP, serial,  or BlueTooth parameters";
        qCritical() << lasterr;
    }
    return false;
} // SendCommandToHost

bool HostInterface::SendDataToHost(QByteArray bytData)
{
    char *MSbyte;
    char *LSbyte;

    if ( blnInitializing ) return false;

    qint32 len = bytData.length();
    MSbyte = new char((len & 0xFF00) >> 8);
    LSbyte = new char(len & 0xFF);

    hid->bytDataToSend->clear();
    hid->bytDataToSend->append(bytData.trimmed());
    hid->bytDataToSend->replace(0,1,"d"); // hexToStr(0x64,2);   "d" indicates data from TNC
    hid->bytDataToSend->replace(1,1,":"); // hexToStr(0x3A,2);   ":"
    hid->bytDataToSend->replace(21,MSbyte);  // MS byte of count  (Includes strDataType but does not include the two trailing CRC bytes)
    hid->bytDataToSend->replace(3,1,LSbyte);         // LS byte of count

    if ( !hid->tcpSocket )
    {
//        Logs.Exception("[SendDataToHost] TCPIPData was Nothing")
        qDebug() << "[SendDataToHost] TCPIPData was Nothing";
        return false;
    }
    QByteArray ba;
    ba.append(hid->bytDataToSend->data());
    hid->tcpSocket->sendCMD(QString(ba));
//        if (hid->CommandTrace ) Logs.WriteDebug("[HostInterface.SendDataToHost] " & GetInfoFromDataCMDSent(bytToSend))
    QString info = GetInfoFromDataCMDSent(*hid->bytDataToSend);
    qDebug() << "[HostInterface.SendDataToHost] " << info;
    return true;
} // SendDataToHost

bool HostInterface::TerminateHostLink()
{
    blnTerminatingConnection = true;
    Status stcStatus;

    stcStatus.ControlName = "lblHost";
    stcStatus.BackColor = COLOR_DARKGRAY;
    if ( hid->strInterfaceType == "TCPIP" )
    {
        try
        {
            if ( hid->tcpSocket )
            {
                delete hid->tcpSocket;
            }
            if ( hid->objCMDClient )
            {
                delete hid->objCMDClient;
            }
            hid->strTCPIPControlConnectionID = "";
            hid->strTCPIPDataConnectionID = "";

            // We leave listening socket, so can accept another connection

            stcStatus.BackColor = COLOR_KHAKAI // preset to yellow ....connection switches to green.
            stcStatus.MsgText = "TCPIP";
        }
        catch (const exception ex)
        {
            stcStatus.BackColor = COLOR_LIGHTSALMON; // set to red ....
            hid->LinkedToHost = false;
//            qDebug() << ex;
            return false;
        }
        hid->LinkedToHost = false;
        return true;
    }
    /*
    else if ( strInterfaceType == "SERIAL" )
    {
    }
    else if ( strInterfaceType == "BLUETOOTH" )
    {
    }
    */
    else
    {
    }
    return false;
}  //TerminateHostLink

void HostInterface::GenCRC16(QByteArray *Data, qint32 intStartIndex, qint32 intStopIndex, qint32 intSeed)
{
    bool blnBit;
    qint32 intPoly;
    qint32 intRegister;
    int val_1;
    int val_2;
    int val_3;
    int i;
    int j;

    Q_UNUSED(val_3) // to quiet warnings

    intPoly = 0x8810;
    intRegister = intSeed;  // passed in as param

    for ( j = intStartIndex; j <= intStopIndex; ++j )
    {
        for ( i = 7; i >= 0; --i )
        {
            // for each bit processing MS bit first
            val_1 = Data[i].toInt();
            val_2 = pow(2.0, double(i));
            val_3 = (intRegister & 0x8000);
            if ( (val_1 & val_2) != 0 )
            {
                blnBit = false;
            }
            else
            {
                blnBit = true;
            }
            if ( val_1 == 0x8000 )
            {
                // the MSB of the register is set
                // Shift left, place data bit as LSB, then divide
                // Register := shiftRegister left shift 1
                // Register := shiftRegister xor polynomial
                if ( blnBit )
                    intRegister = 0xFFFF & (1 + 2 * intRegister);
                else
                    intRegister = 0xFFFF & (2 * intRegister);
                intRegister = intRegister ^ intPoly;
            }
            else
            {
                // the MSB is not set
                // Register is not divisible by polynomial yet.
                // Just shift left and bring current data bit onto LSB of shiftRegister
                if ( blnBit )
                    intRegister = 0xFFFF & (1 + 2 * intRegister);
                else
                    intRegister = 0xFFFF & (2 * intRegister);
            }
        } // Next i
    }  // Next j
    // Put the two CRC bytes after the stop index
    char MSbyte;
    char LSbyte;
    MSbyte = (intRegister & 0xFF00) / 256;
    LSbyte = intRegister  & 0xFF;
    Data[intStopIndex + 1].replace(MSbyte,1); // MS 8 bits of Register
    Data[intStopIndex + 2].replace(LSbyte,1);          // LS 8 bits of Register
} // GenCRC16

//
//
// ============================================================
// FRIENDLY METHODS AND FUNCTIONS
// ============================================================
//                PRIVATE FUNCTIONS                          //
void HostInterface::initialize()
{
    const QString ip = hostip;
    quint16 port = hosttcpport;

    Q_UNUSED(port)

    qDebug() << "Initializing Interface";
    hid->bytDataToSend = new QByteArray;
    hid->bytLastCMD_DataSent = new QByteArray;
    hid->bytDataToSend->clear();
    hid->bytLastCMD_DataSent->clear();
    hid->bytDataToSendLen = 0;

    hid->strInterfaceType = "TCPIP"; // set constant for now
    if ( hid->strInterfaceType == "TCPIP" )
    {
        qDebug() << "Initializing the TCP sockets needed";
        hid->tcpSocket = new TCPIPClient;
    }
}


void HostInterface::processData()
{
}

void HostInterface::commandInterpreter()
{
    // determine if the received data is a command
    // if NOT then it is data so return
    /* Interpret what to do with the command
     * - parse the command by taking the chrs before the 1st space and after the c:
     * - compare to command list
     * - if not on the list return
     * - use switch to get the case involved
     * -   interpret the proper function for this case
     * -   return
     *
     * BTW funtion interpreted may change the interface state and
     * signal the GUI for display change.
     */
     QStringList cmdlist;
     QString cmd;
     QString data;
     qint32 len;
     // parse the data looking for command structure
     cmdlist = hid->currentINData.split(" ",QString::KeepEmptyParts);
     // identify the first element
     cmd = cmdlist.at(0);
     // if there is data then identify as data
     data = cmdlist.at(1);
     // find the length of command including the c:
     len = cmd.length();
     // carve out the actual command without the c: preamble
     cmd = cmd.right(len-2);
     // Now we have the actual command so find out what to do with it.
     qDebug() << "[Command Interpreter] data:" << cmd[0];
     if ( cmd == "GRIDSQUARE" )
     {
         if ( (cmdlist.count() > 1) && (data != "") )
         {  // this is the data from a previous GRIDSQUARE request
             hid->status.ControlName = "txtTNCList";
             hid->status.MsgText = "GRIDSQUARE " + data;
             hid->status.BackColor = QColor(255,255,255,255);
             signalChgDisplay(hid->status);
         }
     }
     else if (cmd[0] ==  "MYCALL" )
     {
         if ( (cmdlist.count() > 1) && (data != "") )
         {
             hid->status.ControlName = "txtTNCList";
             hid->status.MsgText = "MYCALL " + data;
             hid->status.BackColor = QColor(255,255,255,255);
             signalChgDisplay(hid->status);
         }
     }
     else
     {
         return;
     }
}


QByteArray HostInterface::GetBytes(QString strText)
{
    // Function to convert string Text (ASCII) to byte array
    // Converts a text string to a byte array...
    // Since a QString IS a ByteArray this function is provided for
    // backward compatibility only
    quint8 i;
    QByteArray bytBuffer;

    bytBuffer.reserve(strText.trimmed().length());
    for ( i = 0; i < (bytBuffer.length() - 1); ++i )
    {
        bytBuffer.append(strText.mid(i, 1));
    } // intIndex
    return bytBuffer;
} // GetBytes

QString HostInterface::GetString(QByteArray bytBuffer,qint32 intFirst, qint32 intLast)
{
    // Function to Get ASCII string from a byte array
    // Converts a byte array to a text string...
    // Since a QString IS a ByteArray this function is provided for
    // backward compatibility only
    QString sbdInput;
    char bytSingle;

    if ( (intFirst > bytBuffer.length()) == 0 ) return "";
    if ( (intLast == -1) || (intLast > bytBuffer.length()) ) intLast = bytBuffer.length();

    for ( int intIndex = intFirst; intIndex < intLast; ++intIndex )
    {
        bytSingle = bytBuffer.at(intIndex);
        if ( bytSingle != 0 ) sbdInput.append(bytSingle);
    } // next intIndex
    return sbdInput;
}  // GetString

QString HostInterface::GetInfoFromDataCMDSent(QByteArray bytSent)
{
    // Function to extract summary of Info from byte array command or data
    if ( bytSent.length() < 4 ) return "";
    if ( bytSent.at(0) == 0x64 )
    {
        // "d" Data frame
        QString ret = "Data (length = ";
        ret += bytSent.at(2) * 256;
        ret += bytSent.at(3);
        ret += ")";
        return ret;
    }
    else if ( bytSent.at(0) == 0x63 )
    {
        // "c" CMD frame
        if ( hid->strInterfaceType.trimmed() != "TCPIP" )
            bytSent.right(bytSent.length() - 3); // remove the CRC
        return GetString(bytSent);
    }
    else
    {
//        Logs.Exception("[GetInfoFromDataCMDSent] bytSent Len = " & bytSent.Length & "   " & GetString(bytSent).Substring(0, 2))
        qDebug() << "[GetInfoFromDataCMDSent] bytSent Len = " << bytSent.length() << "   " << GetString(bytSent).mid(0, 2);
        return "";
    }
}  // GetInfoFromDataCMDSent

bool HostInterface::CheckGSSyntax(QString strGS)
{
    // Function to check for proper syntax of a 4, 6 or 8 character GS
    quint32 len;
    quint32 i;
    int zero;
    QRegularExpression AU("[A-R]");
    QRegularExpression AL("[a-r]");
    QRegularExpression AF("[A-Z]");
    QRegularExpression N("[0-9");
    QRegularExpressionValidator vA(AU, 0);
    QRegularExpressionValidator vL(AL, 0);
    QRegularExpressionValidator vF(AF, 0);
    QRegularExpressionValidator vN(N, 0);
    QValidator::State test1;
    QValidator::State test2;
    QValidator::State test3;
    QValidator::State test4;
    QString thischr;

    Q_UNUSED(test2)
    Q_UNUSED(test3) // to quiet warnings
    len = strGS.length();
    if ( !(len == 4) || (len == 6) || (len == 8) ) return false;
    test1 = QValidator::Invalid;
    test2 = QValidator::Invalid;
    test3 = QValidator::Invalid;
    test4 = QValidator::Invalid;

    for ( i  = 0; i < (len - 1); ++i )
    {
        thischr = QString(strGS.mid(i, 1).data()->toUpper());
        if ( i < 2 )
        {
            test1 = vA.validate(thischr,zero);
            switch (test1)
            {
            case QValidator::Acceptable:
              return true;
            case QValidator::Intermediate:
              return true;
            default:
              return false;
            }
        }
        else if ( (i > 1) && (i < 4) )
        {
            test2 = vN.validate(thischr,zero);
            switch (test1)
            {
            case QValidator::Acceptable:
                return true;
            case QValidator::Intermediate:
                return true;
            default:
                return false;
            }
        }
        else if ( (i > 3) && (i < 6) )
        {
            test3 = vF.validate(thischr,zero);
            switch (test1)
            {
            case QValidator::Acceptable:
                return true;
            case QValidator::Intermediate:
                return true;
            default:
                return false;
            }
        }
        else
        {
            test4 = vN.validate(thischr,zero);
            switch (test4)
            {
            case QValidator::Acceptable:
                return true;
            case QValidator::Intermediate:
                return true;
            default:
                return false;
            }
        }
    }  // Next i
    return true;
}  //  CheckGSSyntax







// ============================================================
// LOCAL APPLICATION SETTINGS PERSISTENT STORAGE
// ============================================================
void HostInterface::getSettings()
{
    QSettings settings("ardop.ini", QSettings::IniFormat);

    settings.beginGroup(QLatin1String("NETWORK"));
      // first get settings into local private vars
      id = settings.value("DefaultNetworkConfiguration","").toString();
      hid->hosttcpaddr = settings.value("HostIP","127.0.0.1").toString();
      hid->hosttcpport = settings.value("HostTCPPort","8515").toInt();
      hid->hostudpport = settings.value("HostUDPPort","8516").toInt();
      // now update property vars
      hostname = "localhost";
      hostip = hid->hosttcpaddr;
      hosttcpport = hid->hosttcpport;
      hostudpport = hid->hostudpport;
      if ( hid->initializing ) hid->CONNECTED = false;
    settings.endGroup();
    qDebug() << "HostIP: " << hid->hosttcpaddr;
    qDebug() << "HostTCPPort: " << hid->hosttcpport;
    qDebug() << "HostUDPPort: " << hid->hostudpport;


    settings.beginGroup(QLatin1String("ARDOP"));
      hid->strInterfaceType = settings.value("InterfaceType","TCPIP").toString();
      hid->AccumulateStats = settings.value("EnableAccumulateStats",false).toBool();
      hid->Use600Modes  = settings.value("Use600Modes",false).toBool();
      hid->FSKOnly      = settings.value("EnableFSKOnly",false).toBool();
      hid->fastStart    = settings.value("EnableFastStart",false).toBool();
      hid->skip167      = settings.value("Skip167",false).toBool();
      hid->ConsoleLogLevel = settings.value("ConsoleLogLevel",false).toBool();
      hid->EnablePingAck   = settings.value("EnablePingAck",false).toBool();
      hid->blnPINGrepeating = settings.value("EnablePingRepeating",false).toBool();
      hid->blnFramePending  = settings.value("EnableFrameRepeating",false).toBool();
      hid->ARQTimeout = settings.value("ARQTimeout",30).toInt();
      hid->bytDataToSendLen = settings.value("DataToSendLen",0).toInt();
      hid->LeaderLength  = settings.value("LeaderLength",0).toInt();
      hid->TrailerLength = settings.value("TrailerLength",0).toInt();
      hid->TuningRange   = settings.value("TuningRange",0).toInt();
      hid->TXLevel       = settings.value("TXLevel",80).toInt();
      hid->RXLevel       = settings.value("RXLevel",60).toInt();
      hid->autoRXLevel   = settings.value("AutoRXLevel",60).toInt();
      hid->ARQConReqRepeats = settings.value("ARQConReqRepeats",5).toInt();
      hid->dttLastPINGSent  = settings.value("LastPINGSentTime",0).toInt();
      hid->intPINGRepeats   = settings.value("PINGrepeats",0).toInt();
    settings.endGroup();


    settings.beginGroup(QLatin1String("STATION"));
      hid->wantCWID = settings.value("CWID",false).toBool();
      hid->CWOnOff  = settings.value("CWIDonoff",false).toBool();
      hid->DebugLog = settings.value("EnableDebugLog",false).toBool();
      hid->CommandTrace = settings.value("EnableCommandTrace",false).toBool();
      hid->RadioControl = settings.value("EnableRadioControl",false).toBool();
      hid->SlowCPU      = settings.value("SlowCPU",false).toBool();
      hid->GridSquare    = settings.value("GridSquare","XXNNxx").toString();
      hid->Callsign      = settings.value("CallSign","AA0AAA").toString();
      hid->strFECMode    = settings.value("FECMode","Undef").toString();
    settings.endGroup();

    settings.beginGroup(QLatin1String("SOUND"));
      hid->CaptureDevice = settings.value("CaptureDevice","Default").toString();
      hid->PlaybackDevice = settings.value("PlaybackDevice","").toString();
    settings.endGroup();

    /*  uncomment and edit when implemented
    settings.beginGroup(QLatin1String("RIGCONTROL"));
      hid->gotGPIO      = settings.value("GotGPIO",false).toBool();
      hid->useGPIO      = settings.value("EnableGPIO",false).toBool();
      hid->pttGPIOPin    = settings.value("pttGPIOPin",0).toInt();
      hid->hRIGDevice    = settings.value("RigDevice",0).toInt();
      hid->RIGBAUD       = settings.value("RIGBAUD",9600).toInt();
      hid->hPTTDevice    = settings.value("PTTPort",0).toInt();
      hid->RIGPORT       = settings.value("RigPort","00").toString();
      hid->PTTPORT       = settings.value("PTTport","00").toString();
    settings.endGroup();
    */
}

void HostInterface::saveSettings()
{
    QSettings settings("ardop.ini", QSettings::IniFormat);

    settings.beginGroup(QLatin1String("NETWORK"));
      settings.setValue("DefaultNetworkConfiguration", id);
      settings.setValue("HostIP",hostip);
      settings.setValue("HostName","localhost");
      settings.setValue("HostUDPPort",hostudpport);
    settings.endGroup();

    settings.beginGroup(QLatin1String("ARDOP"));
      settings.setValue("InterfaceType", hid->strInterfaceType);
      settings.setValue("EnableAccumulateStats",hid->AccumulateStats);
      settings.setValue("Use600Modes",hid->Use600Modes);
      settings.setValue("EnableFSKOnly",hid->FSKOnly);
      settings.setValue("EnableFastStart",hid->fastStart);
      settings.setValue("Skip167",hid->skip167);
      settings.setValue("ConsoleLogLevel",hid->ConsoleLogLevel);
      settings.setValue("EnablePingAck",hid->EnablePingAck);
      settings.setValue("EnablePingRepeating",hid->blnPINGrepeating);
      settings.setValue("EnableFrameRepeating",hid->blnFramePending);
      settings.setValue("ARQTimeout",hid->ARQTimeout);
      settings.setValue("DataToSendLen",hid->bytDataToSendLen);
      settings.setValue("HostTCPport",hid->hosttcpport);
      settings.setValue("HostUDPport",hid->hostudpport);
      settings.setValue("LeaderLength",hid->LeaderLength);
      settings.setValue("TrailerLength",hid->TrailerLength);
      settings.setValue("TuningRange",hid->TuningRange);
      settings.setValue("TXLevel",hid->TXLevel);
      settings.setValue("RXLevel",hid->RXLevel);
      settings.setValue("AutoRXLevel",hid->autoRXLevel);
      settings.setValue("ARQConReqRepeats",hid->ARQConReqRepeats);
      settings.setValue("LastPINGSentTime",hid->dttLastPINGSent);
      settings.setValue("PINGrepeats",hid->intPINGRepeats);
    settings.endGroup();


    settings.beginGroup(QLatin1String("STATION"));
      settings.setValue("CWID",hid->wantCWID);
      settings.setValue("CWIDonoff",hid->CWOnOff);
      settings.setValue("EnableDebugLog",hid->DebugLog);
      settings.setValue("EnableCommandTrace",hid->CommandTrace);
      settings.setValue("EnableRadioControl",hid->RadioControl);
      settings.setValue("SlowCPU",hid->SlowCPU);
      settings.setValue("GridSquare",hid->GridSquare);
      settings.setValue("CallSign",hid->Callsign);
      settings.setValue("FECMode",hid->strFECMode);
    settings.endGroup();

    settings.beginGroup(QLatin1String("SOUND"));
      settings.setValue("CaptureDevice",hid->CaptureDevice);
      settings.setValue("PlaybackDevice",hid->PlaybackDevice);
    settings.endGroup();

    /* uncomment and edit when implemented
    settings.beginGroup(QLatin1String("RIGCONTROL"));
      settings.setValue("GotGPIO",hid->gotGPIO);
      settings.setValue("EnableGPIO",hid->useGPIO);
      settings.setValue("pttGPIOPin",hid->pttGPIOPin);
      settings.setValue("RigDevice",hid->hRIGDevice);
      settings.setValue("RIGBAUD",hid->RIGBAUD);
      settings.setValue("PTTPort",hid->hPTTDevice);
      settings.setValue("RigPort",hid->RIGPORT);
      settings.setValue("PTTport",hid->PTTPORT);
    settings.endGroup();
    */
    settings.sync();
}
