#include <QApplication>
#include <QHostAddress>

#include "tcpclient.hpp"

TCPIPSocket::TCPIPSocket(QObject *parent) : QObject(parent)
{
    error = 0;
    errorstr = "";
}

void TCPIPSocket::slotSocketError(QAbstractSocket::SocketError e)
{
    errorstr = "";
    switch ( e )
    {
    case QAbstractSocket::AddressInUseError:
      {
        error = socket->error();
        errorstr = "TCP Port Address already in use";
      }
        break;
    case QAbstractSocket::ConnectionRefusedError:
      {
        error = socket->error();
        errorstr = "TCP Host refused connection";
      }
        break;
    case QAbstractSocket::DatagramTooLargeError:
      {
        error = socket->error();
        errorstr = "Datagram to large";
      }
        break;
    case QAbstractSocket::HostNotFoundError:
      {
        error = socket->error();
        errorstr = "Specified Host Not Found";
      }
        break;
    case QAbstractSocket::NetworkError:
      {
        error = socket->error();
        errorstr = "Unspecified Network Error";
      }
        break;
    case QAbstractSocket::OperationError:
      {
        error = socket->error();
        errorstr = "Network Operation Error";
      }
        break;
    case QAbstractSocket::PauseOnSslErrors:
//    case QAbstractSocket::RemoteHostClosedError: same value as PauseOnSslErrors
      {
        error = socket->error();
        errorstr = "Network SSL Pause Error or Remote Host Closed";
      }
        break;
    case QAbstractSocket::ProxyAuthenticationRequiredError:
        break;
    case QAbstractSocket::ProxyConnectionClosedError:
        break;
    case QAbstractSocket::ProxyConnectionRefusedError:
        break;
    case QAbstractSocket::ProxyConnectionTimeoutError:
        break;
    case QAbstractSocket::ProxyNotFoundError:
        break;
    case QAbstractSocket::ProxyProtocolError:
        break;
    case QAbstractSocket::SocketAccessError:
      {
        error = socket->error();
        errorstr = "Socket Access Error";
      }
        break;
//    case QAbstractSocket::SocketError:
//        haserror = true;
//        error = socket.error();
//        errorstr = "Unspecified Socket Error";
//        break;
    case QAbstractSocket::SocketResourceError:
      {
        error = socket->error();
        errorstr = "Socket Resource Error";
      }
        break;
    case QAbstractSocket::SocketTimeoutError:
      {
        error = socket->error();
        errorstr = "Socket Timeout Error";
      }
        break;
    case QAbstractSocket::SslHandshakeFailedError:
        break;
    case QAbstractSocket::SslInternalError:
        break;
    case QAbstractSocket::SslInvalidUserDataError:
        break;
    case QAbstractSocket::TemporaryError:
      {
        error = socket->error();
        errorstr = "Temporary Network Error";
      }
        break;
    case QAbstractSocket::UnfinishedSocketOperationError:
      {
        error = socket->error();
        errorstr = "Unfinished Operation Error";
      }
        break;
    case QAbstractSocket::UnknownSocketError:
      {
        error = socket->error();
        errorstr = "Unknown Socket Error";
      }
        break;
    case QAbstractSocket::UnsupportedSocketOperationError:
      {
        error = socket->error();
        errorstr = "Unsupported Socket Operation Error";
      }
        break;
    default:
      {
        errorstr = socket->errorString();
        if ( errorstr != "")
        {
            error = socket->error();
            qCritical() << errorstr;
        }
      }
        break;
    }
    qCritical() << errorstr;
    if ( errorstr != "" ) signalSocketError();
}

void TCPIPSocket::slotConnected()
{
    qDebug() << "CONNECTED...";
}

void TCPIPSocket::slotDisconnected()
{
    qDebug() << "disconnected...";
}

void TCPIPSocket::slotBytesWritten(qint64 bytes)
{
    qDebug() << bytes << " bytes written...";
    qDebug() << lastba;
}

void TCPIPSocket::slotReadyRead()
{
    qDebug() << "reading...";

    // read the data from the socket
    inData = socket->readAll();
    qDebug() << "In data :" << inData;
    signalInData();
    QApplication::processEvents();
}

QString TCPIPSocket::getInData()
{
    return inData;
}

void TCPIPSocket::connectHost(QString ip, qint32 port)
{
    socket = new QTcpSocket(this);
    connect(socket, SIGNAL(connected()),this, SLOT(slotConnected()));
    connect(socket, SIGNAL(disconnected()),this, SLOT(slotDisconnected()));
    connect(socket, SIGNAL(bytesWritten(qint64)),this, SLOT(slotBytesWritten(qint64)));
    connect(socket, SIGNAL(readyRead()),this, SLOT(slotReadyRead()));
    connect(socket, SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(slotSocketError(QAbstractSocket::SocketError)));

    qDebug() << "Making a non-blocking connection";
    socket->connectToHost(ip, port);

    if ( !socket->waitForConnected(5000) )
    {
        qDebug() << "Waiting for CONNECTED exceeds 5 seconds...";
        error = socket->error();
        errorstr = socket->errorString();
        qDebug() << "Error: " << socket->errorString();
    }
    QApplication::processEvents();
}

void TCPIPSocket::writeBytes(QByteArray ba)
{
    qDebug() << "Writing to port: " << ba;
    lastba.clear();
    lastba.append(ba);

    if ( socket->write(ba) == ba.size() )
        errorstr = "Socket error on write of " + ba + " error- " + socket->errorString();
    else
        qDebug() << lastba << "Written to port  with no error";
}

