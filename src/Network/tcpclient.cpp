#include <unistd.h>

#include <QApplication>
#include <QString>
#include <QSettings>
#include <QTimer>
#include <QByteArray>
#include <QDataStream>
#include <QTcpSocket>
#include <QNetworkSession>
#include <QNetworkConfigurationManager>

#include "tcpclient.hpp"

class TCPIPClient::ClientPrivate
{
public:
    bool        initializing;
    bool        CONNECTED;

    quint32     hosttcpport;
    quint32     hostudpport;

    QString     id;
    QString     hosttcpaddr;
    QString     msgInData;

    QByteArray  lastba;

    QDataStream in;

    QNetworkConfigurationManager manager;

    QTcpSocket      *tcpSocket;
    QNetworkSession *networkSession;
};

TCPIPClient::TCPIPClient(QObject* parent) : QObject(parent)
{
    c = new ClientPrivate;
    readSettings();

	init();
}

TCPIPClient::~TCPIPClient()
{
    c->tcpSocket->deleteLater();
    delete c->networkSession;
    delete c;
}

// ====================================================
// EVENT HANDLERS
// ====================================================
// ====================================================
// PUBLIC SLOT HANDLERS
// ====================================================
// ====================================================
// PRIVATE SLOT HANDLERS
// ====================================================
void TCPIPClient::slotReadyRead()
{
    QByteArray ba;
    QTimer t;

    ba.clear();
    c->in.startTransaction();

    c->in >> ba;

    if ( !c->in.commitTransaction() ) return;
    if ( c->lastba == QString(ba) )
    {
        usleep(10);
        c->tcpSocket->read(512);
        return;
    }

    c->lastba = ba;
    qDebug() << "Data Read:" << QString(ba).toLocal8Bit();
}

void TCPIPClient::slotSocketError(QAbstractSocket::SocketError e)
{
    switch ( e )
    {
    case QAbstractSocket::SocketAddressNotAvailableError:
        qDebug() << "General Socket Error";
        break;
    case QAbstractSocket::AddressInUseError:
        qDebug() << "Connected IP Address already in use";
        break;
    case QAbstractSocket::ConnectionRefusedError:
        qDebug() << "Connection Refused By Host";
        break;
    case QAbstractSocket::DatagramTooLargeError:
        qDebug() << "Datagram Too Large Error";
        break;
    case QAbstractSocket::HostNotFoundError:
        qDebug() << "TCPIP Host Address not found";
        break;
    case QAbstractSocket::OperationError:
        qDebug() << "IP Operation Invalid";
        break;
    case QAbstractSocket::NetworkError:
        qDebug() << "General Network Error";
        break;
    case QAbstractSocket::RemoteHostClosedError:
        qDebug() << "Remote Host Closed";
        break;
    case QAbstractSocket::SocketAccessError:
        qDebug() << "Socket Access error";
        break;
    case QAbstractSocket::SocketResourceError:
        qDebug() << "Socket Resource Error";
        break;
    case QAbstractSocket::SocketTimeoutError:
        qDebug() << "Socket Timeout Error";
        break;
    case QAbstractSocket::UnfinishedSocketOperationError:
        qDebug() << "Unfinished Socket Operation Error";
        break;
    default:
        qDebug() << "Unspecified Error" << c->tcpSocket->errorString();
        break;
    }
}

void TCPIPClient::slotSessionOpened()
{
    // Save the used configuration
    QNetworkConfiguration config = c->networkSession->configuration();
    if (config.type() == QNetworkConfiguration::UserChoice)
      c->id = c->networkSession->sessionProperty(QLatin1String("UserChoiceConfiguration")).toString();
    else
      c->id = config.identifier();
    saveSettings();
}

// ====================================================
// PUBLIC METHODS AND FUNCTIONS
// ====================================================
QString TCPIPClient::getInData()
{
    return c->msgInData;
}

QAbstractSocket::SocketState TCPIPClient::getState()
{
    QString sktStateStr;
    QTcpSocket::SocketState sktState;
    sktState = c->tcpSocket->state();
    // DEBUGING SCRIPT - REMOVE BEFORE RELEASE
    if ( sktState == QTcpSocket::BoundState ) sktStateStr = "BOUND SOCKET";
    if ( sktState == QTcpSocket::ClosingState ) sktStateStr = "CLOSING";
    if ( sktState == QTcpSocket::ConnectedState ) sktStateStr = "CONNECTED";
    if ( sktState == QTcpSocket::ConnectingState ) sktStateStr = "CONNECTING";
    if ( sktState == QTcpSocket::HostLookupState ) sktStateStr = "HOST LOOKUP";
    if ( sktState == QTcpSocket::ListeningState ) sktStateStr = "LISTENING";
    if ( sktState == QTcpSocket::UnconnectedState ) sktStateStr = "UNCONNECTED";
    qDebug() << "Socket State before write" << sktStateStr;
    return c->tcpSocket->state();
}


// ====================================================
// PRIVATE FRIENDLY FUNCTIONS
// ====================================================
void TCPIPClient::sendCMD(QString cmd)
{
    c->tcpSocket->abort();
    qDebug() << "Connecting to host " << c->hosttcpaddr << ":" << QString("%1").arg(c->hosttcpport);
    c->tcpSocket->connectToHost(QHostAddress(c->hosttcpaddr),c->hosttcpport);
    connect(c->tcpSocket,SIGNAL(stateChanged(QAbstractSocket::SocketState)),this,SLOT(getState()));
    QApplication::setOverrideCursor(Qt::ArrowCursor);
    c->tcpSocket->write(cmd.toLatin1());
}


void TCPIPClient::init()
{
    c->networkSession = Q_NULLPTR;
    c->tcpSocket = new QTcpSocket(this);
    connect(c->tcpSocket, SIGNAL(readyRead()), this, SLOT(slotReadyRead()));
    connect(c->tcpSocket, SIGNAL(disconnected()), this, SLOT(deleteLater()));
    if (c->manager.capabilities() &
        QNetworkConfigurationManager::NetworkSessionRequired
       )
    {
        // If the saved network configuration is not currently discovered use the system default
        QNetworkConfiguration config = c->manager.configurationFromIdentifier(c->id);
        if ( (config.state() & QNetworkConfiguration::Discovered) !=
              QNetworkConfiguration::Discovered
           )
        {
            config = c->manager.defaultConfiguration();
        }

        c->networkSession = new QNetworkSession(config, this);
        connect(c->networkSession, SIGNAL(opened()), this, SLOT(slotSessionOpened()));

        qDebug() << "Opening network session...";
        c->networkSession->open();
    }
}

// ============================================================
// LOCAL APPLICATION SETTINGS PERSISTENT STORAGE
// ============================================================
void TCPIPClient::readSettings()
{
    QSettings settings("ardop.ini", QSettings::IniFormat);

    settings.beginGroup(QLatin1String("NETWORK"));
      // first get settings into local private vars
      c->id = settings.value("DefaultNetworkConfiguration","").toString();
      c->hosttcpaddr = settings.value("HostIP","127.0.0.1").toString();
      c->hosttcpport = settings.value("HostTCPPort","8515").toInt();
      c->hostudpport = settings.value("HostUDPPort","8516").toInt();
      if ( c->initializing ) c->CONNECTED = false;
    settings.endGroup();
    qDebug() << "HostIP: " << c->hosttcpaddr;
    qDebug() << "HostTCPPort: " << c->hosttcpport;
    qDebug() << "HostUDPPort: " << c->hostudpport;
}

void TCPIPClient::saveSettings()
{
    QSettings settings("ardop.ini", QSettings::IniFormat);

    settings.beginGroup(QLatin1String("NETWORK"));
      settings.setValue("DefaultNetworkConfiguration", c->id);
      settings.setValue("HostIP",c->hosttcpaddr);
      settings.setValue("HostName","localhost");
      settings.setValue("HostUDPPort",c->hostudpport);
    settings.endGroup();

    settings.sync();
}
