#include <QDebug>

#include "serverconnection.hpp"

ServerConnection::ServerConnection(QTcpSocket *connection, QObject* parent) : QObject(parent)
{
	header = NULL;
	clientConnection = connection;
	connect(clientConnection, SIGNAL(readyRead()), this, SLOT(readFromSocket()));
    connect(clientConnection, SIGNAL(disconnected()), this, SLOT(deleteLater()));
}

ServerConnection::~ServerConnection()
{
	clientConnection->deleteLater();
	delete header;
}

void ServerConnection::readFromSocket()
{
	QString lastLine;

	while ( clientConnection->canReadLine() && !header )
    {
		lastLine = clientConnection->readLine();
		headerString += lastLine;
		if ( lastLine == "\r" )
        {
            if ( header->contentLength() <= clientConnection->bytesAvailable() )
            {   // all data complete
                parseReturn(clientConnection->readAll());
                return;
            }
        }
    }
}

void ServerConnection::sendCMD(QString content)
{
	QString block;
	block.append(QString("C:").toUtf8();
	block.append(content.toUtf8());
    block.append(QString("\r").toUtf8());

    clientConnection->write(block);
	clientConnection->disconnectFromHost();
}

void ServerConnection::parseReturn(QString ret)
{
    QStringList retstr;
    retstr << QString(ret).split(" ",QString::KeepEmptyParts);

    QString methodName = retstr[0];
	switch ( methodName )
    {
    case "VERSION":
        // send to host the parsed response and data
        emit getMethod(methodName, &objResponse, &dataResponse);
        break;
    }
    default:
    { /* recieved invalid response */
		qWarning() << "Invalid response received";
        return;
	}
}


bool ServerConnection::invokeMethodWithVariants(QObject *obj,
			const QByteArray &method, const QVariantList &args,
			QVariant *ret, Qt::ConnectionType type)
{
	if (args.count() > 10) return false;

	return true;
}


