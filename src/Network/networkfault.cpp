#include "networkfault.hpp"

NetworkFault::MaiaFault(const NetworkFault &other) : QObject(other.parent())
{
	fault = other.fault;
}

NetworkFault::NetworkFault(int faultCode,
                           QString faultString,
                           QObject *parent)
             : QObject(parent)
{
	fault["faultCode"] = faultCode;
	fault["faultString"] = faultString;
}

QString NetworkFault::toString()
{
	return faultString;
}
