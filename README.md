# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Re-imagine of the curses version of ARIM
* Current version is v0.5.0

### How do I get set up? ###

* Absolutely necessary to use Qt Framework v5.n
* See system dependencies for Qt in the install docs
* No other dependencies yet
* No Database is used yet
* External tessting is not implemented
* There will be a installer creaated when released

### Contribution guidelines ###

* Contact Project leader 
* This is an Open Source project licensed with GPL v3 or higher
* Other guidelines

### Who do I talk to? ###

* AD5XJ, Ken ad5xj@arrl.net
* Project consultation with Bob Cummings, NW8L