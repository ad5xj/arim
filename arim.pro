TEMPLATE = app
TARGET = arimd
#TARGET = arim # use for relase version
DESTDIR = bin
DESTDIR_TARGET = bin

QT += \
    widgets \
#    serialport \
    network

CONFIG += qt thread C++11 silent
CONFIG += qt exceptions debug  # comment out for release


MOC_DIR     = .moc
RCC_DIR     = .rcc
OBJECTS_DIR = .obj
UI_DIR      = .ui
UI_HEADERS_DIR = .ui

INCLUDEPATH += \
    inc \
    inc/network \
#    inc/serial  \
#    inc/bluetooth \
    ui

HEADERS += \
    inc/globals.hpp \
    inc/utils.hpp

HEADERS += \
    inc/network/tcpclient.hpp
#HEADERS += \
#    inc/serial/serialclient.hpp
#HEADERS += \
#    inc/bluetooth/bluetoothclient.hpp

HEADERS += \
    inc/helpabout.hpp \
    inc/hostinterface.hpp \
    inc/arimwindow.hpp


SOURCES += \
    src/Network/tcpclient.cpp

SOURCES += \
    src/helpabout.cpp \
    src/hostinterface.cpp \
    src/arimwindow.cpp \
    main.cpp


FORMS += \
    ui/helpabout.ui \
    ui/arimwindow.ui

RESOURCES += 

DISTFILES += \
    bin/ardop.ini


