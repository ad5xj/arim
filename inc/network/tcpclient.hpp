#ifndef TCPCLIENT_HPP
#define TCPCLIENT_HPP
#include <QObject>
#include <QAbstractSocket>

class QTcpSocket;
class QAbstractSocket;
class QNetworkSession;
class QDataStream;

class TCPIPClient : public QObject
{
    Q_OBJECT

public:
    explicit TCPIPClient(QObject *parent = 0);
    ~TCPIPClient();

    qint32  errno;
    QString errorstr;
    QString inData;

    void sendCMD(QString cmd);
    QString getInData();


signals:
    void signalSocketError();
    void signalInData();

public slots:

private slots:
    void slotSocketError(QAbstractSocket::SocketError e); //! responds to TCPIP socket error signal
    void slotReadyRead();
    void slotSessionOpened();       //! responds to session opened() signal
    QAbstractSocket::SocketState getState();

private:
    class ClientPrivate;
    ClientPrivate *c;

    void init();
    void readSettings();
    void saveSettings();
};

#endif // TCPCLIENT_HPP