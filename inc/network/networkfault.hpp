#ifndef NETWORKFAULT_HPP
#define NETWORKFAULT_HPP
#include <QObject>

class NetworkFault : public QObject
{
	Q_OBJECT

public:
		NetworkFault(int faultCode = 0,
                     QString faultString = QString(),
                     QObject *parent = 0
                    );
		NetworkFault(const NetworkFault &other);

        QString toString();
		QMap<QString,QVariant> fault;
};
Q_DECLARE_METATYPE(NetworkFault)

#endif // NETWORKFAULT_HPP
