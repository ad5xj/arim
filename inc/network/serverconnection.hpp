#ifndef SERVERCONNECTION_HPP
#define SERVERCONNECTION_HPP
#include <QObject>
#include <QTcpSocket>

class ServerConnection : public QObject
{
	Q_OBJECT

	public:
		ServerConnection(QTcpSocket *connection, QObject *parent = 0);
		~ServerConnection();

	signals:
		void getMethod(QString msg, QObject **objResponse, const char **slotResponse);  //! signal from host with response and data

	private slots:
		void readFromSocket();

	private:
        QString headerString;

        QTcpSocket *clientConnection;

        void sendCMD(QString cmd);
		void parseReturn(QString ret);
};

#endif // SERVERCONNECTION_HPP
