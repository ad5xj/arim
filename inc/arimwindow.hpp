#ifndef ARIMWINDOW_HPP
#define ARIMWINDOW_HPP
#include <QObject>
#include <QColor>
#include <QWidget>
#include <QMainWindow>

#define TITLE_ROW                   1
#define STATUS_TIMER_COUNT          25
#define WIN_CHANGE_TIMER_COUNT      10

#define LT_HEARD_CLOCK              0
#define LT_HEARD_ELAPSED            1

#define TITLE_REFRESH               1
#define TITLE_TNC_DETACHED          2
#define TITLE_TNC_ATTACHED          3

#define MAX_DATA_BUF_LEN          512
#define MAX_DATA_ROW_SIZE         512
#define MAX_CMD_SIZE              512
#define MAX_HEARD_SIZE            512
#define DATA_BUF_SCROLLING_TIME   300
#define MAX_CMD_HIST              15+1
#define MAX_HEARD_LIST            30
#define MAX_HEARD_LIST_LEN        31

//    ARIM STATUS DEFINES          //
#define STATUS_REFRESH              1
#define STATUS_WAIT_ACK             2
#define STATUS_WAIT_RESP            3
#define STATUS_BEACON_SENT          4
#define STATUS_NET_MSG_SENT         5
#define STATUS_MSG_ACK_RCVD         6
#define STATUS_MSG_NAK_RCVD         7
#define STATUS_RESP_RCVD            8
#define STATUS_ACK_TIMEOUT          9
#define STATUS_RESP_TIMEOUT         10
#define STATUS_FRAME_START          11
#define STATUS_FRAME_END            12
#define STATUS_ARIM_FRAME_TO        13
#define STATUS_RESP_SEND            14
#define STATUS_ACKNAK_SEND          15
#define STATUS_MSG_RCVD             16
#define STATUS_NET_MSG_RCVD         17
#define STATUS_QUERY_RCVD           18
#define STATUS_MSG_REPEAT           19
#define STATUS_RESP_SENT            20
#define STATUS_ACKNAK_SENT          21
#define STATUS_MSG_SEND_CAN         22
#define STATUS_QRY_SEND_CAN         23
#define STATUS_RESP_SEND_CAN        24
#define STATUS_ACKNAK_SEND_CAN      25
#define STATUS_BCN_SEND_CAN         26
#define STATUS_SEND_UNPROTO_CAN     27
#define STATUS_RESP_WAIT_CAN        30
#define STATUS_FRAME_WAIT_CAN       31
#define STATUS_MSG_START            32
#define STATUS_QRY_START            33
#define STATUS_RESP_START           34
#define STATUS_BCN_START            35
#define STATUS_MSG_END              36
#define STATUS_QRY_END              37
#define STATUS_RESP_END             38
#define STATUS_BCN_END              39
#define STATUS_PING_RCVD            40
#define STATUS_PING_SENT            41
#define STATUS_PING_ACK_RCVD        42
#define STATUS_PING_SEND_CAN        43
#define STATUS_PING_ACK_TIMEOUT     44
#define STATUS_PING_MSG_SEND        45
#define STATUS_PING_MSG_ACK_BAD     46
#define STATUS_PING_MSG_ACK_TO      47
#define STATUS_PING_QRY_SEND        48
#define STATUS_PING_QRY_ACK_TO      49
#define STATUS_PING_QRY_ACK_BAD     50
#define STATUS_ARQ_CONN_REQ         51
#define STATUS_ARQ_CONN_CAN         52
#define STATUS_ARQ_CONNECTED        53
#define STATUS_ARQ_DISCONNECTED     54
#define STATUS_ARQ_CONN_REQ_SENT    55
#define STATUS_ARQ_CONN_REQ_TO      56
#define STATUS_ARQ_CONN_PP_SEND     57
#define STATUS_ARQ_CONN_PP_ACK_TO   58
#define STATUS_ARQ_CONN_PP_ACK_BAD  59
#define STATUS_ARQ_FILE_RCV_WAIT    60
#define STATUS_ARQ_FILE_RCV         61
#define STATUS_ARQ_FILE_RCV_DONE    62
#define STATUS_ARQ_FILE_RCV_ERROR   63
#define STATUS_ARQ_FILE_SEND        64
#define STATUS_ARQ_FILE_SEND_DONE   65
#define STATUS_ARQ_FILE_SEND_ACK    66
#define STATUS_ARQ_MSG_RCV          67
#define STATUS_ARQ_MSG_RCV_DONE     68
#define STATUS_ARQ_MSG_RCV_ERROR    69
#define STATUS_ARQ_MSG_SEND         70
#define STATUS_ARQ_MSG_SEND_ACK     71
#define STATUS_ARQ_MSG_SEND_DONE    72
//                                  //

#define COLOR_DARKGRAY    QColor(150,150,150,255)
#define COLOR_KHAKAI      QColor(230,115,0,255)
#define COLOR_LIGHTSALMON QColor(255,179,102,255)
#define COLOR_YELLOW      QColor(255,255,0,255)

#include "hostinterface.hpp"
#include "ui_arimwindow.h"

class QAbstractSocket;

namespace Ui {
class ARIMWindow;
}

class ARIMWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ARIMWindow();
    ~ARIMWindow();

private slots:
    void slotHelpAbout();
    void slotDisplayTCPError(int error, QString socketErrorStr);
    void slotChgDisplay(Status status);
    void slotSendHostCMD();

private:
    qint32 heard_list_cnt;

    class ARIMPrivate;
    // Objects on the heap   //
    ARIMPrivate    *d;
    Ui::ARIMWindow *ui;

    void readSettings();
    void saveSettings();
    // see note in implementation code
    void get_heard_list(QString *listbuf, qint32 listbufsize);
    void cmd_func(QByteArray *data);
};
#endif // ARIMWINDOW_HPP
