#ifndef GLOBALS_HPP
#define GLOBALS_HPP
#include <time.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <math.h>

#include <QObject>
#include <QColor>

#define ProductName "ARDOPq TNC"
#define ProductVersion "0.5.0.0-LinBPQ"

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows XP or later.
# define _WIN32_WINNT 0x0501	// Change this to the appropriate value to target other versions of Windows.
#endif

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
# define _CRT_SECURE_NO_DEPRECATE
#define _USE_32BIT_TIME_T

//	Sound interface buffer size
#define SendSize 1200		// 100 mS for now
#define ReceiveSize 240		// try 50mS 100 mS for now
#define NumberofinBuffers 4


#ifndef TEENSY
# ifndef WIN32
#  define LINUX
# endif
#endif

#ifdef __ARM_ARCH
# ifndef TEENSY
#  define ARMLINUX
# endif
#endif


// TEENSY Interface board equates
#ifdef TEENSY
# ifdef PIBOARD
#  define ISSLED LED0
# else
#  define ISSLED LED1
# endif
# define IRSLED LED1
# define TRAFFICLED LED2
#else
# define ISSLED 0
# define IRSLED 0
# define TRAFFICLED 0
#endif
// ARIM SPECIFIC DEFINES //
#define ST_NULL          0
#define ST_PIPE_1        1
#define ST_TYPE          2
#define ST_VERSION       3
#define ST_PIPE_2        4
#define ST_FROM_CALL     5
#define ST_PIPE_3        6
#define ST_TO_CALL       7
#define ST_PIPE_4        8
#define ST_SIZE          9
#define ST_GRIDSQ        10
#define ST_PIPE_5        11
#define ST_CHECK         12
#define ST_PIPE_6        13
#define ST_MSG           14
#define ST_BEACON_END    15
#define ST_MSG_END       16
#define ST_ACK_END       17
#define ST_NAK_END       18
#define ST_QUERY_END     19
#define ST_RESPONSE_END  20
#define ST_UNPROTO       21
#define ST_ERROR         99
//                       //


// Packet Header flags //
#define BREAK      0x23
#define IDLEFRAME  0x24
#define DISCFRAME  0x29
#define END        0x2C
#define ConRejBusy 0x2D
#define ConRejBW   0x2E
#define ConAck200  0x39
#define ConAck500  0x3A
#define ConAck1000 0x3B
#define ConAck2000 0x3C
#define PINGACK    0x3d
#define PING       0x3E
//                     //

#define PTTRTS		1
#define PTTDTR		2
#define PTTCI_V		4		// Not used here (but may be later)

enum _ReceiveState		// used for initial receive testing...later put in correct protocol states
{
	SearchingForLeader,
	AcquireSymbolSync,
	AcquireFrameSync,
	AcquireFrameType,
	DecodeFrameType,
	AcquireFrame,
	DecodeFramestate
};
extern _ReceiveState RcvState;

enum _ARQBandwidth
{
	B200FORCED,
	B500FORCED,
	B1000FORCED,
	B2000FORCED,
	B200MAX,
	B500MAX,
	B1000MAX,
	B2000MAX,
	UNDEFINED
};
extern _ARQBandwidth ARQBandwidth;

enum _ARDOPState
{
	OFFLINE,
	DISC,
	ISS,
	IRS,
	IDLE,     // ISS in quiet state ...no transmissions)
	IRStoISS, // IRS during transition to ISS waiting for ISS's ACK from IRS's BREAK
 	FECSend,
	FECRcv
};
extern _ARDOPState ARDOPState;

// Enum of ARQ Substates may need to move to ARQ module
enum _ARQSubStates
{
	None,
	ISSConReq,
	ISSConAck,
	ISSData,
	ISSId,
	IRSConAck,
	IRSData,
	IRSBreak,
	IRSfromISS,
	DISCArqEnd
};
extern _ARQSubStates ARQState;

// Enum of ARDOP Protocol mode may need to move to PROTOCOL layer module
enum _ProtocolMode
{
	Undef,
	FEC,
	ARQ
};
extern _ProtocolMode ProtocolMode;

struct SEM
{
	quint32 Flag;
	qint32  Clashes;
	qint32	Gets;
	qint32  Rels;
};

/*!
 * \struct Status
 * \details
 * Structure for passing TNC status via synchronous queue<br/>
 * Structure for passing interface form control updates via
 * thread safe synchronized queue... not necessary for Qt but provided
 *  for backward compatibility
 */
struct Status
{
    QString ControlName;
    QString MsgText;
    qint32  MsgValue;
    QColor  BackColor;
}; // Status



// may not be needed with QT //
void txSleep(int mS);
unsigned int getTicks();
#define Now getTicks()
//                           //

#endif // GLOBALS_HPP
