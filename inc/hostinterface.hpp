#ifndef HOSTINTERFACE_HPP
#define HOSTINTERFACE_HPP
/*! /file hostinterface.hpp  */
/*! /ingroup INTERFACE */
#include <QObject>
#include <QByteArray>
#include <QTcpSocket>
#include <QUdpSocket>
#include <QHostInfo>
#include <QDataStream>

typedef QAbstractSocket::SocketError Socket_Error;

/* uncomment when implemented as needed.
 * Must add serialport and / or bluetooth to QT += in .pro
 * NOTE: USB port is not a serial port without an adapter
#include <QSerialPort>
#include <QBluetoothAddress>
#include <QBluetoothDevice>
#include <QBluetoothLocalDevice>
#include <QBluetoothDeviceInfo>
#include <QBluetoothHostInfo>
#include <QBluetoothDeviceDiscoveryAgent>
#include <QBluetoothServer>
*/

#include "globals.hpp"

#define MAX_CMD_SIZE        256
#define MAX_DATA_SIZE     16384
#define MAX_MSG_LINE_SIZE  1024
#define MAX_ARIM_HDR_SIZE    64
#define MAX_MSG_SIZE      MAX_DATA_SIZE
#define MIN_DATA_BUF_SIZE MAX_DATA_SIZE+1
#define MIN_MSG_BUF_SIZE  MAX_ARIM_HDR_SIZE+MAX_MSG_SIZE+1

/*! \ingroup INTERFACE
 * \brief Client side HostInterface class
 * \details
 * This class defines the control and data interface
 * to the host (ARDOP) for a client. There are a defined number of commands
 * sent or received to and from the host. See the command interpreter
 * docs or the ARDOP docs for the valid list.<br/><br/>
 * This module is not a clone of the VB.NET ARDOP project. It is
 * completely redesigned to be functionally similar but
 * exploiting all the benefits of Qt and rejecting the inefficiencies of VB
 * for multiple platforms.<br/><br />
 *
 * Command Interface<br/><br/>
 * The command interface is via ASCII commands sent to the
 * TCPIP address/port, the serial port or to the Bluetooth paired device
 * identified in the host .ini file.   It is designed for a computerized
 * host interface (not keyboard commands as in 1980&rsquo;s type Packet
 * TNCs).  For robustness all serial or Bluetooth commands and data are
 * protected by a 16 bit CRC.  TCPIP commands and data are not CRC
 * protected as the TCPIP protocol itself has built in CRC and auto
 * repeat if necessary. All commands are plain (readable) text using
 * 7 bit ASCII character encoding. Commands sent from the host begin
 * with &ldquo;C:&rdquo;  and terminate with a <Cr> followed by a 2 byte
 * CRC check for non TCPIP interfaces. No command name or parameter may
 * include the strings &ldquo;C:&rdquo; , &ldquo;c:&rdquo;, or the
 * <Cr> character.  Commands that do not include parameters will cause
 * an &ldquo;echo back&rdquo; of the command and current parameters as
 * described below. Commands or replies from the TNC always begin with
 * &ldquo;c:&rdquo; (lower case c) and end with a &ldquo;<Cr>+2 byte CRC&rdquo;
 * (no CRC used for TCPIP connections).  Commands and data are no longer
 * acknowledged by a &ldquo;RDY&rdquo; reponse as in prior versions. All
 * commands and data will respond with an echo of the command followed by
 * the current value as below:<br/><br/>
 * Host CMD Sent:   				TNC Response:<br/>
 * Command sent to query current value:<br/>
 * C:ARQBW(Cr)				        c:ARQBW 2000MAX (Cr)<br/>
 * Command sent to change current value:<br/>
 * C:ARQBW 1000MAX(Cr)			    c:ARQBW NOW 1000MAX(Cr)<br/>
 * Response to command that neither accepts or returns a value:<br/>
 * C:ABORT(Cr)				        c:ABORT(Cr)<br/><br/>
 * All commands and data are buffered and processed on a first in first
 * out basis. Host programs should normally sequence commands based on
 * the resposes above.  If a command is incorrect or received corrupted
 * it will prompt a FAULT response allowing the host program to repeat
 * the command or take alternate action. Command execution and reply is
 * usually < 100 ms but a few commands may take a few seconds to execute.<br><br>
 * When a command from the host is received with improper syntax or
 * parameter values the Virtual TNC will reply with &ldquo;c:FAULT
 * (fault description + Echo back of command)(CR)+2 byte CRC&rdquo;
 * A CRC fault (not used with a TCPIP connection) should prompt the
 * Host to repeat the command.<br/><br/>
 * When a command from the host is received with improper syntax or
 * parameter values the Virtual TNC will reply with
 * &ldquo;c:FAULT (fault description + Echo back of command)(CR)+2 byte
 * CRC&rdquo; A CRC fault (not used with a TCPIP connection) should
 * prompt the Host to repeat the command.                     */
class HostInterface : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool haserror READ hasError)
    Q_PROPERTY(bool datawaiting READ dataWaiting)
    Q_PROPERTY(quint32 intftype READ getInterfaceType WRITE setInterfaceType)
    Q_PROPERTY(QString lasterr READ getLastError)
    /* uncomment when implemented
      Q_PROPERTY(QString serialPort  READ getSerialPort WRITE setSerialPort)
      Q_PROPERTY(QString blueaddr    READ getBluetoothAddress WRITE setBluetoohAddress)
      Q_PROPERTY(QString bluedev     READ getBluetoothDevice  WRITE setBluetoohDevice)
      Q_PROPERTY(QString blueldev    READ getBluetoothLocalDevice  WRITE setBluetoohLocalDevice)
    */
    // to externally set/get saved setting values
    // as a property of the host interface object
    Q_PROPERTY(QString hostname    READ getHostName WRITE setHostName)
    Q_PROPERTY(QString hostip      READ getHostIP   WRITE setHostIP)
    Q_PROPERTY(quint32 hosttcpport READ getTCPport  WRITE setTCPport)
    Q_PROPERTY(quint32 hostudpport READ getUDPport  WRITE setUDPport)

public:
    explicit HostInterface(QObject *parent = nullptr);
    ~HostInterface();

    // PROPERTIES //
    /*! \details
     * Interface types are TCPIP=1, SERIAL=2, BLUETOOTH=3
     * Default is TCPIP with UDP channel one port higher (e.g. 8515 / 8516 ) */
    void setInterfaceType(quint32 t)  { intftype = t; }
    void setHostIP(QString i)         { hostip = i;   }
    void setHostName(QString n)       { hostname = n; }
    void setTCPport(quint32 p)        { hosttcpport = p; hostudpport = hosttcpport + 1; }
    void setUDPport(quint32 p)        { hostudpport = p; } // probably not needed but provided here
    /* uncomment when implemented
      void setSerialPort(QString p)     { serialPort = p; }
      void setBluetoothAddress(QString a) { blueaddr = b; }
      void setBluetoothDevice(QString d) { blueaddr = d; }
      void setBluetoothLocalDevice(QString d) { blueldev = d; }
    */

    bool hasError()            { return haserror; }
    bool dataWaiting()         { return datawaiting; } //! Semaphore for data received but not fetched. Set to true if signalDataAvailable() is sent.  reset to false when data is picked up
    quint32 getInterfaceType() { return intftype; }
    quint32 getTCPport()       { return hostudpport; }
    quint32 getUDPport()       { return hostudpport; }
    QString getLastError()     { return lasterr; }
    QString getHostName()      { return hostname; }
    QString getHostIP()        { return hostip; }
    /* uncomment when implemented
    QString getSerialPort()    { return serialPort; }
    QString getBluetoothAddress() { return blueaddr; }
    QString getBluetoothDevice() { return bluedev; }
    QString getBluetoothLocalDevice() { return blueldev; }
    */
    // END PROPERTIES  //


    // PUBLIC VARS     //
    int     errorno;          //! error number from the captured error state
    QString lasterr;          //! error string from the last captured error


    /*! /details
     * This function sends a messsage to the host interface as chosen.  */
    bool out(QByteArray msg);

    /*! /details
     * Function to send a text command to the Host
     * This is from TNC side as identified by the leading "c:"
     * (Host side sends "C:")
     *
	 * Subroutine to send a line of text (terminated with <Cr>) on
	 * the command port... All commands beging with "c:" and end with <Cr>
	 * A two byte CRC appended following the <Cr>
	 * The strText cannot contain a "c:" sequence or a <Cr>
	 * Returns TRUE if command sent successfully.
	 * Form byte array to send with CRC   */
    bool SendCommandToHost(QString strText);

    /*! /details
     * This is from TNC side as identified by the leading "d:"
     * (Host side sends data with leading  "D:")
     * for non TCPIP includes 16 bit CRC check on Data Len + Data
     * (does not CRC the leading "d:")
     * bytData contains a "tag" in its leading 3 bytes: "ARQ", "FEC" or
     * "ERR" which are examined and stripped by Host (optionally used by
     * host for display)
     *
     * Max data size should be 2000 bytes or less for timing purposes */
    bool SendDataToHost(QByteArray bytData);
    bool TerminateHostLink();  //! Function to terminate the Host link

    QString getDataWaiting();  //! called in response to signalDataAvailable()


signals:
    void signalHasData();     //! tnc has data for the host
    void signalConnectReq();  //! tnc has connect request received
    void signalConnected();   //! tnc has data connect received
    void signalChgDisplay(Status status); //! signal to GUI to update display
    void signalDataAvailable(QString data); // latest data in is ready for pickup

public slots:

private slots:
    void slotReadData();

private:
    // PROPERTY VARS     //
    bool    haserror;    //! initialized to false. Set to true on error.
    bool    datawaiting; //! Semaphore for data received but not fetched. Set to true if signalDataAvailable() is sent.  reset to false when data is picked up

    quint32 intftype;
    quint32 hosttcpport;  //! the tcpip port exposed from settings or set for storage in settings.
    quint32 hostudpport;  //! always one more than the TCP port


    QString hostip;
    QString hostname;
    /* uncomment when implemented
      QString serialPort;
      QString blueaddr;
      QString bluedev;
      QString blueldev;
    */
    // END PROPERTY VARS //

    bool blnInitializing;
    bool blnTerminatingConnection;

    QString id;

    // local *d-ptr definition  //
    class HostInterfaceData;
    // local var interface
    HostInterfaceData *hid;
    //                          //


    /*! \details
     * Subroutine to compute a 16 bit CRC value and append it to the Data...
     * For  CRC-16-CCITT = x^16 + x^12 +x^5 + 1  intPoly = 1021 Init FFFF
     * intSeed is the seed value for the shift register and must be in the range 0-&HFFFF
     */
    void GenCRC16(QByteArray *Data, qint32 intStartIndex, qint32 intStopIndex, qint32 intSeed = 0xFFFF);

    /*! \details
     * When the in-data processor determines data is a command, this function will
     * interpret what to do with that command by sending the appropriate signal and/or
     * data to the GUI. NOTE: The CI can change the state of the interface but not the
     * application or display.
     */
    void commandInterpreter();

    void initialize();
    void processData();
    void saveSettings();
    void getSettings();

    bool CheckGSSyntax(QString strGS);

    QByteArray GetBytes(QString strText);
    QString    GetString(QByteArray bytBuffer,qint32 intFirst = 0, qint32 intLast = -1);
    QString    GetInfoFromDataCMDSent(QByteArray bytSent);
};

#endif // HOSTINTERFACE_H
