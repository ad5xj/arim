/*!
 * \copyright 2017 AD5XJ
 * contact: Ken ad5xj@arrl.net
 *
 * The GPL License was included in the source code files
 * of this project. If you did not receive a copy of the
 * license, please got to http://fsf.org for a current
 * version or write to:<br />
 * Free Software Foundation, Inc.<br />
 * 59 Temple Place - Suite 330,<br />
 * Boston, MA  02111-1307, USA.<br /><br />
 *
 * $QT_BEGIN_LICENSE:GPL$
 * You may use this file under the terms of the GPL v3
 * or later license as follows:
 *
 * Redistribution and use in source and binary forms, with or
 * without modification, are permitted provided that the following
 * notice is included with the source code:
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 * CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $QT_END_LICENSE$   */
/*! \file main.cpp */
/*! \ingroup QNETLOG  */
#include <stdio.h>
#include <stdlib.h>

#include <QDebug>
#include <QDateTime>
#include <QTranslator>
#include <QMessageLogger>
#include <QMessageLogContext>
#include <QFile>
#include <QTextStream>
#include <QSettings>
#include <QStyleFactory>
#include <QApplication>

#include "globals.hpp"
#include "arimwindow.hpp"


void debugMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    bool    dbg2file;
    QString dbgfile;
    dbg2file = false;
    dbgfile  = "";

    QSettings settings("arim.ini",QSettings::IniFormat);
    settings.beginGroup("Config");
     dbg2file = settings.value("Dbg2File",false).toBool();
     dbgfile  = settings.value("DbgFile","debug.log").toString().trimmed();
    settings.endGroup();

//    QString txt = QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss ");
    // Switch structure left to be converted to write
    // into the file in the future
    QString txt = "";
    switch ( type )
    {
    case QtDebugMsg:
        txt += " [Debug] ";
        txt += QString(context.function);
        txt += ":";
        txt += QString("%1").arg(context.line);
        txt += "  --  ";
        txt += QString(" %1").arg(msg);
        break;
    case QtWarningMsg:
        txt += " [Warning:] ";
        txt += QString(context.function);
        txt += ":";
        txt += QString("%1").arg(context.line);
        txt += "  --  ";
        txt += QString(" %1").arg(msg);
        break;
    case QtCriticalMsg:
        txt += " [Critical:] ";
        txt += QString(context.function);
        txt += ":";
        txt += QString("%1").arg(context.line);
        txt += "  --  ";
        txt += QString(" %1").arg(msg);
        break;
    case QtInfoMsg:
        txt += " [INFO]: ";
        txt += QString(context.function);
        txt += ":";
        txt += QString("%1").arg(context.line);
        txt += "  --  ";
        txt += QString(" %1").arg(msg);
        break;
    case QtFatalMsg:
        txt += " <<<Fatal>>>: ";
        txt += QString(context.function);
        txt += ":";
        txt += QString("%1").arg(context.line);
        txt += "  --  ";
        txt += QString(" %1").arg(msg);
    }

    if ( dbg2file )
    {
        QFile outFile(dbgfile);
        outFile.open(QIODevice::WriteOnly | QIODevice::Append);

        QTextStream textStream(&outFile);
        textStream << txt << endl;
    }
    else
    {
        qDebug() << txt;
    }
}



int main(int argc, char *argv[])
{
    qInstallMessageHandler(debugMessageOutput);
    QApplication a(argc, argv);
    QTranslator translator;
    a.installTranslator(&translator);
    qDebug() << "loading from main";

    ARIMWindow w;
    a.setStyle(QStyleFactory::create("Fusion"));

    w.show();
    int ret = a.exec();
    return ret;
}

